using System.Diagnostics;
using System.IO;

namespace FuelAdvance.PreviewHandlerPack.PreviewHandlers
{
	public abstract class StreamBasedPreviewHandlerControl : PreviewHandlerControl
	{
		public sealed override void Load(FileInfo file)
		{
			var htmlFilePath = Path.Combine(file.DirectoryName, "index.html");
			using (var fs = new FileStream(htmlFilePath, FileMode.Open, FileAccess.Read, FileShare.Delete | FileShare.ReadWrite))
			{
				//Debugger.Launch();
				LoadFromPath(htmlFilePath);
			}
		}
	}
}