using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace FuelAdvance.PreviewHandlerPack.PreviewHandlers.Handlers
{
    public class CodePreviewHandlerControl : StreamBasedPreviewHandlerControl
    {
        readonly string definition = string.Empty;

        public CodePreviewHandlerControl(string definition)
        {
            this.definition = definition;
        }

        public override void Load(Stream stream)
        {
            //Debugger.Launch();
            var sourceHtml = HighlightHelpers.GetHighlightedHtml(definition, stream);

            var webBrowser = new WebBrowser
            {
                Dock = DockStyle.Fill,
                DocumentText = sourceHtml
            };
            Controls.Add(webBrowser);
        }

        public override void LoadFromPath(string filePath)
        {
            //Debugger.Launch();
            //var sourceHtml = HighlightHelpers.GetHighlightedHtml(definition, stream);

            var webBrowser = new WebBrowser
            {
                Dock = DockStyle.Fill,
                Url = new Uri($"file:///{filePath}")
            };
            Controls.Add(webBrowser);
        }
    }
}